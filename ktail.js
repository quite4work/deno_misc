import { MuxAsyncIterator } from "https://deno.land/std@0.148.0/async/mux_async_iterator.ts";
import { includes, notIncludes } from "./strings.js";
import { getPods, tailContainerLogs } from "./kubernetes.js";

export async function* ktail(opts = {}) {
  const logs = new MuxAsyncIterator();
  logs.add(genTails(logs, opts));
  yield* logs;
}

async function* genTails(logs, opts) {
  for await (const p of watchPods(opts)) {
    const {
      type,
      object: {
        spec: { containers, nodeName: host },
        metadata: {
          namespace,
          name: pod,
          deletionTimestamp,
        },
      },
    } = p;
    switch (type) {
      case "ADDED":
        for (const { name } of containers) {
          logs.add(tailContainer(p, name, opts));
        }
        break;
      case "MODIFIED":
        yield {
          pod,
          host,
          namespace,
          ts: new Date(),
          type: "podModify",
        };
        break;
      case "DELETED":
        yield {
          pod,
          host,
          namespace,
          ts: new Date(deletionTimestamp),
          type: "podDelete",
        };
        break;
      default:
        yield {
          pod,
          host,
          namespace,
          ts: new Date(),
          type: "error",
          msg: `error: unknown type: ${type}`,
        };
    }
  }
}

// NOTE: follow kubectl defaults
async function runTail(pod, container, opts = {}) {
  const { since } = opts;
  let sinceSeconds;
  const hours = since.match(/^(\d+)h$/);
  if (hours) {
    sinceSeconds = new Number(hours[1]) * 60 * 60;
  }
  const min = since.match(/^(\d+)m$/);
  if (min) {
    sinceSeconds = new Number(min[1]) * 60;
  }
  const sec = since.match(/^(\d+)s$/);
  if (sec) {
    sinceSeconds = new Number(sec[1]);
  }
  return await tailContainerLogs({
    ...opts,
    pod,
    follow: true,
    timestamps: true,
    container,
    sinceSeconds,
    // previous: true,
  });
}

async function* watchPods(opts = {}) {
  const { namespace, host, pod, notPod, notHost } = opts;
  const res = await getPods({ ...opts, watch: true });
  for await (const msg of res) {
    const {
      object: { metadata: { namespace: ns, name }, spec: { nodeName } },
    } = msg;

    const filter = includes(ns, namespace) &&
      includes(nodeName, host) &&
      includes(name, pod) &&
      notIncludes(name, notPod) &&
      notIncludes(nodeName, notHost);
    if (filter) {
      yield msg;
    }
  }
}

async function* tailContainer(p, container, opts) {
  const {
    object: {
      metadata: { namespace, name: pod },
      spec: { nodeName: host },
    },
  } = p;
  const tail = await runTail(pod, container, { ...opts, namespace });
  let lastTs;
  let start = true;
  for await (const msg0 of tail) {
    const g = msg0.split(/\s(.*)/s);
    const ts = new Date(g[0]);
    lastTs = ts;
    const msg = g[1];
    if (start) {
      yield { pod, container, host, namespace, ts, type: "logStart" };
      start = false;
    }
    yield { pod, container, host, namespace, msg, ts, type: "logMsg" };
  }
  yield { pod, container, host, namespace, ts: lastTs, type: "logEnd" };
}
