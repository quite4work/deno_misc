import { isRegExp } from "https://cdn.skypack.dev/lodash-es@4.17.21";

export function re(strings, ...keys) {
  let result = strings[0];
  for (let i = 1; i < strings.length; i++) {
    const k = keys[i - 1];
    if (isRegExp(k)) {
      result += k.source;
    } else if (typeof k === "function") {
      result += k().source;
    } else if (typeof k === "object") {
      const [[k2, v]] = Object.entries(k);
      let v2;
      if (isRegExp(v)) {
        v2 = v.source;
      } else if (typeof v === "function") {
        v2 = v().source;
      } else {
        v2 = v.toString();
      }
      result += `(?<${k2}>${v2})`;
    } else {
      result += k.toString();
    }
    result += strings[i];
  }
  return new RegExp(result);
}

const year = () => /[12][0-9]{3}/;
const month = () => /(?:0[1-9]|1[0-2])/;
const day = () => /(?:0[1-9]|[1-2][0-9]|3[0-1])/;
const hours = () => /(?:0[0-9]|1[0-9]|2[0-4])/;
const minutes = () => /(?:[0-5][0-9])/;
const seconds = minutes;
export const date = () => re`${year}-${month}-${day}`;
export const time = () => re`${hours}:${minutes}:${seconds}(?:\\.[0-9]{1,9})?`;
export const dateTime = () => re`${date}[_T ]${time}`;
export const file = () => /[\w_]+\.\w+/;

const weekday = () =>
  /(?:[Mm]on|[Tt]ue|[Ww]ed|[Tt]hu|[Ff]ri|[Ss]at|[Ss]un|MON|TUE|WED|THU|FRI|SAT|SUN)/;
const monthName = () =>
  /(?:[Jj]an|[Ff]eb|[Mm]ar|[Aa]pr|[Mm]ay|[Jj]un|[Jj]ul|[Aa]ug|[Ss]ep|[Oo]ct|[Nn]ov|[Dd]ec)/;

const ts1 = () =>
  re`${weekday}\\s+${monthName}\\s+${day}\\s+${time}\\s+${year}`;

export const anyTs = () => re`(?:${ts1}|${time}|${dateTime})`;

// based on https://github.com/PrismJS/prism/blob/master/components/prism-log.js
const string = () => /"(?:[^"\\\r\n]|\\.)*"|'(?![st] | \w)(?:[^'\\\r\n]|\\.)*'/;
const uuid = () =>
  /\b[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\b/i;
const filePath = () =>
  /\b[a-z]:[\\/][^\s|,;:(){}\[\]"']+|(^|[\s:\[\](>|])\.{0,2}\/\w[^\s|,;:(){}\[\]"']*/i;

// https://github.com/golang/glog/blob/master/glog.go#L524
// Lmmdd hh:mm:ss.uuuuuu threadid file:line] msg...
export function klog() {
  const level = /[A-Z]/;
  const line = /\d+/;
  const msg = /.*/;
  return re`${{ level }}${{ month }}${{ day }}\\s+${{ time }}\\s+\\d+\\s+${{
    file,
  }}:${{
    line,
  }}\\]\\s+${{ msg }}`;
}

export function log() {
  return re`(${{ string }}|${{ uuid }}|${{ filePath }}|.)*`;
}

export function logfmt() {
  const string = /"((\\)+"|[^"])*"/;
  const val = re`(${string}|[^\\s"]*)`;
  return re`^\\s*(\\w+=${val})(\\s\\w+=${val})+\\s*$`;
}
