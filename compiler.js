import {
  basename,
  dirname,
  extname,
  fromFileUrl,
  join,
} from "https://deno.land/std@0.148.0/path/posix.ts";
import { denoPlugin } from "https://deno.land/x/esbuild_deno_loader@0.5.0/mod.ts";
import { build } from "https://deno.land/x/esbuild@v0.14.49/mod.js";

// NOTE: wasm code slows deno start

// NOTE: emit contains wasm
// import * as emit from "https://deno.land/x/emit@0.4.0/mod.ts";

// NOTE: esbuild contains wasm:
// https://deno.land/x/denoflate@1.2.1/pkg/denoflate_bg.wasm.js

export function compilerYargsCommands(opts) {
  return [{
    command: "bundle",
    desc: "",
    handler: (argv) => bundleMainModule({ ...opts, ...argv }),
    builder: {},
  }, {
    command: "compile",
    desc: "",
    handler: (argv) => compileMainModule({ ...opts, ...argv }),
    builder: {},
  }];
}

async function bundleMainModule(opts = {}) {
  const { importMap, suffix = "-bundle" } = opts;
  const src = fromFileUrl(Deno.mainModule);
  const out = join(dirname(src), basename(src, extname(src))) + suffix;
  const res = await Deno.spawn(Deno.execPath(), {
    args: [
      "bundle",
      "--no-check",
      ...(importMap ? [`--import-map=${importMap}`] : []),
      src,
      out,
    ],
  });
  if (!res.status.success) {
    throw new TextDecoder().decode(res.stderr);
  }
  const file = `#!/usr/bin/env deno run --unstable -A\n\n` +
    await Deno.readTextFile(out);
  await Deno.writeTextFile(out, file);
  const res2 = await Deno.spawn("chmod", { args: ["+x", out] });
  if (!res2.status.success) {
    throw new TextDecoder().decode(res2.stderr);
  }
}

async function compileMainModule(opts = {}) {
  const { importMap, suffix = "-bin" } = opts;
  const src = fromFileUrl(Deno.mainModule);
  const out = join(dirname(src), basename(src, extname(src))) + suffix;
  const res = await Deno.spawn(Deno.execPath(), {
    args: [
      "compile",
      "--no-check",
      "-A",
      "--unstable",
      ...(importMap ? [`--import-map=${importMap}`] : []),
      "-o",
      out,
      src,
    ],
  });
  if (!res.status.success) {
    throw new TextDecoder().decode(res.stderr);
  }
  const res2 = await Deno.spawn("chmod", { args: ["+x", out] });
  if (!res2.status.success) {
    throw new TextDecoder().decode(res2.stderr);
  }
}

export async function startWorker(modulePath, initString) {
  const dir = dirname(modulePath);
  const tmp = await Deno.makeTempFile({ dir, suffix: ".js" });

  const src = await Deno.readTextFile(modulePath) + "\n\n" + initString;
  await Deno.writeTextFile(tmp, src);

  const worker = await bundle(tmp);
  await Deno.remove(tmp);

  new Worker(`data:application/javascript,${encodeURIComponent(worker)}`, {
    type: "module",
  });
}

export async function bundleWeb(modulePath, opts = {}) {
  const { out } = opts;
  const { outputFiles } = await build({
    plugins: [denoPlugin()],
    entryPoints: [modulePath],
    format: "esm",
    bundle: true,
    sourcemap: "inline",
    write: out ? true : false,
    target: ["safari13"],
    outfile: out ?? "bundle.js",
  });
  if (!out) {
    return new TextDecoder().decode(outputFiles[0].contents);
  }
}
