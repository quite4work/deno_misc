import ColorHash from "https://deno.land/x/color_hash@v2.0.1/mod.ts";
import css from "https://gitlab.com/xdeno/utils/-/raw/0.0.1/literal.js";
import { Button, Checkbox, Input } from "./react.jsx";
import { includes } from "./strings.js";
import { delay } from "https://deno.land/std@0.146.0/async/delay.ts";
import { VariableSizeGrid } from "https://esm.sh/react-window@1.8.7";
import React from "https://esm.sh/react@18.1.0";
import ReactDOM from "https://esm.sh/react-dom@18.1.0";
import Highlighter from "https://esm.sh/react-highlight-words@0.18.0";
import CreatableSelect from "https://esm.sh/react-select@5.3.1/creatable?deps=react@18.1.0,react-dom@18.1.0";
import { components } from "https://esm.sh/react-select@5.3.1?deps=react@18.1.0,react-dom@18.1.0";
import { DateTimeFormatter } from "https://deno.land/std@0.137.0/datetime/formatter.ts";
import { localStorageProxy, proxySet, useSnapshot } from "./valtio.js";
import { TabPanel, Tabs } from "./reactTabs.jsx";
import {
  Tab,
  TabList,
} from "https://esm.sh/react-tabs@5.1.0?deps=react@18.1.0,react-dom@18.1.0";
import {
  SortableContainer,
  SortableElement,
  SortableHandle,
} from "https://esm.sh/react-sortable-hoc@2.0.0?deps=react@18.1.0,react-dom@18.1.0";
import { isEqual } from "https://cdn.skypack.dev/lodash-es@4.17.21";

const textShadow =
  "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000";
const style = css`
  body {
    white-space: nowrap;
    margin: 0;
    height: 100%;
    overflow: hidden
  }
  .host {
    white-space : nowrap;
    overflow : hidden;
    background-color: #f0f0f0;
    user-select: none;
    -webkit-user-select: none;
  }
  .level {
    white-space : nowrap;
    overflow : hidden;
    color: #ffffff;
    background-color: #000000;
    user-select: none;
    -webkit-user-select: none;
  }
  .inferredLevel {
    white-space : nowrap;
    overflow : hidden;
    color: #ffffff;
    background-color: #000000;
    user-select: none;
    -webkit-user-select: none;
  }
  .pod {
    white-space : nowrap;
    overflow : hidden;
    user-select: none;
    -webkit-user-select: none;
    color: white;
    text-shadow: ${textShadow};
  }
  .container {
    white-space : nowrap;
    overflow : hidden;
    user-select: none;
    -webkit-user-select: none;
    background-color: #f0f0f0;
  }
  .ts {
    white-space : nowrap;
    overflow : hidden;
    background-color: #e0f0ff;
  }
  .pod:hover {
    background-color: #f0ffff;
  }
  .container:hover {
    background-color: #f0ffff;
  }
  .host:hover {
    background-color: #f0ffff;
  }
  .ts:hover {
    background-color: #f0ffff;
  }
  .level:hover {
    background-color: #f0ffff;
  }
  .inferredLevel:hover {
    background-color: #f0ffff;
  }
  .host:hover {
    user-select: auto;
    -webkit-user-select: text;
  }
  .pod:hover {
    user-select: auto;
    -webkit-user-select: text;
  }
`;

const colorHash = new ColorHash();

globalThis.addEventListener("scroll", function () {
  window.lastScrollTime = new Date().getTime();
});

const log = [];
let processedLog = [];

const persist = [
  "filters",
  "isAnd",
  "isNot",
  "searchText",
  "searchRegex",
  "sortSelected",
  "selected",
];
const state = localStorageProxy({
  name: "app",
  persist: ["history", ...persist],
  initialState: {
    history: [],
    filters: [],
    isAnd: false,
    isNot: false,
    follow: true,
    init: true,
    searchText: "",
    searchRegex: false,
    rowsAdded: 0,
    renderTime: 0,
    isMenuOpen: false,
    sortSelected: [],
    select: {
      host: proxySet(),
      pod: proxySet(),
      level: proxySet(),
      inferredLevel: proxySet(),
      container: proxySet(),
    },
    selected: {
      host: {},
      pod: {},
      level: {},
      inferredLevel: {},
      container: {},
      undefined: {},
    },
    refresh: 0,
  },
});

function onFollow() {
  state.follow = !state.follow;
}

function onSort() {
  processedLog = processLog(log.sort((a, b) => a.ts - b.ts));
  state.refresh++;
}

function onScroll({ scrollUpdateWasRequested }) {
  if (
    state.follow && !scrollUpdateWasRequested && processedLog.length > 100
  ) {
    state.follow = false;
  }
}

function onInput({ target: { value } }) {
  let text = value;
  let regex = false;
  const match = value.match(/^\/(.+)\/$/);
  if (match) {
    text = new RegExp(match[1]);
    regex = true;
  }
  state.searchText = text;
  state.searchRegex = regex;
}

function onChange(options) {
  const { selected } = state;
  state.sortSelected = options;
  state.filters = options
    .filter(({ __isNew__ }) => __isNew__)
    .map(({ value }) => value);
  for (const group of Object.keys(selected)) {
    const values = options
      .filter((log) => log.group === group)
      .map((
        { value },
      ) => {
        const value2 = group === "level" ? value.toLowerCase() : value;
        if (!selected[group][value2]) {
          selected[group][value2] = { checked: true };
        }
        return value;
      });
    for (const value of Object.keys(selected[group])) {
      if (!values.includes(value)) {
        delete selected[group][value];
      }
    }
  }
  processedLog = processLog(log);
  state.refresh++;
}

function onKeyDown(event) {
  switch (event.key) {
    case "Tab":
      event.preventDefault();
      break;
    case "Enter":
  }
}

const styles = {
  option: (styles, { data }) => (
    {
      ...styles,
      textShadow,
      color: "white",
      backgroundColor: colorHash.hex(data.value + "a"),
      padding: 0,
    }
  ),
  multiValue: (styles, { data }) => (
    {
      ...styles,
      textShadow,
      color: "white",
      backgroundColor: colorHash.hex(data.value + "a"),
    }
  ),
  multiValueLabel: (styles, { data }) => ({
    ...styles,
    textShadow,
    color: "white",
    backgroundColor: colorHash.hex(data.value + "a"),
  }),
};

function connect(opts) {
  const { port } = opts;
  const ws = new WebSocket(`ws://localhost:${port}`);
  ws.onerror = async () => {
    console.log("Waiting WebSocket");
    await delay(100);
    connect(opts);
  };
  ws.onmessage = (e) => {
    const row = JSON.parse(e.data);
    row.ts = new Date(row.ts);
    log.push(row);
  };
}

function initFun(opts) {
  const histObj = Object.fromEntries(persist.map((k) => [k, state[k]]));
  const { ts, ...last } = state.history[state.history.length - 1];
  if (!isEqual(histObj, last)) {
    histObj.ts = new Date();
    state.history.push(histObj);
  }
  connect(opts);
  let lastLogLength = log.length;
  let lastCalledTime = performance.now();
  function renderLoop() {
    const delta = performance.now() - lastCalledTime;
    lastCalledTime = performance.now();
    const added = log.length - lastLogLength;
    if (added > 0) {
      state.rowsAdded = added;
      state.renderTime = Math.floor(delta);
      for (let i = lastLogLength; i < log.length; i++) {
        const { pod, container, host, level, inferredLevel } = log[i];
        processedLog.push(...processLog([log[i]]));
        state.select.pod.add(pod);
        state.select.container.add(container);
        state.select.host.add(host);
        state.select.level.add(level);
        state.select.inferredLevel.add(inferredLevel);
      }
      state.refresh++;
    }
    lastLogLength = log.length;
    requestAnimationFrame(renderLoop);
  }
  renderLoop();
}

function styleLevel(level, colName) {
  switch (level) {
    case "info":
      return { color: "#00FF00" };
    case "error":
      if (colName === "level") {
        return {
          color: "black",
          backgroundColor: "red",
        };
      } else {
        return { color: "red" };
      }
    case "warning":
      if (colName === "level") {
        return {
          color: "black",
          backgroundColor: "orange",
        };
      } else {
        return { color: "orange" };
      }
    case "debug":
      return { color: "magenta" };
  }
}

function processRow(row) {
  const { pod, level, inferredLevel, ts } = row;
  const colsData = {
    level: { style: styleLevel(level, "level") },
    inferredLevel: { style: styleLevel(inferredLevel, "inferredLevel") },
    pod: { style: { backgroundColor: colorHash.hex(pod + "a") } },
  };
  const level2 = (level ? level[0] : "").toUpperCase();
  const inferredLevel2 = inferredLevel ? inferredLevel[0] : "";

  const ts2 = new DateTimeFormatter("HH:mm:ss.SSS MM-dd")
    .format(ts, { timeZone: "UTC" });
  return {
    ...row,
    level: level2,
    inferredLevel: inferredLevel2,
    ts: ts2,
    colsData,
  };
}

function Cell({ data, rowIndex, columnIndex, style }) {
  const row = data[rowIndex];
  const colName = row.cols[columnIndex];
  const col = row[colName];
  const colStyle = row.colsData[colName]?.style ?? {};
  const style2 = { ...style, fontFamily: "monospace", ...colStyle };

  const { searchText, searchRegex, filters } = useSnapshot(state);

  if (colName === "msg") {
    return (
      <Highlighter
        style={style2}
        className={colName}
        highlightClassName="YourHighlightClass"
        searchWords={[...filters, searchText]}
        autoEscape={!searchRegex}
        textToHighlight={col}
      />
    );
  } else {
    return <div style={style2} className={colName}>{col}</div>;
  }
}

function processLog(log) {
  return filterLog(log).map(processRow);
}

function filterLog(log) {
  const {
    selected,
    filters,
    isAnd,
    isNot,
  } = state;

  let log2 = log;

  for (const group of Object.keys(selected)) {
    log2 = Object.keys(selected[group]).length > 0
      ? log2.filter((log) => selected[group][log[group]]?.checked)
      : log2;
  }

  if (isAnd) {
    for (const filter of filters) {
      log2 = filters.length > 0
        ? log2.filter(({ msg }) => {
          const res = includes(msg, filter);
          return isNot ? !res : res;
        })
        : log2;
    }
  } else {
    log2 = filters.length > 0
      ? log2.filter(({ msg }) => {
        const res = includes(msg, filters);
        return isNot ? !res : res;
      })
      : log2;
  }
  return log2;
}

function Status() {
  const { rowsAdded, renderTime } = useSnapshot(state);
  return <span>{log.length} (+{rowsAdded}, {renderTime}ms)</span>;
}

function onCheckboxChange(group, value) {
  const { selected: { [group]: { [value]: { checked } } } } = state;
  state.selected[group][value].checked = !checked;
  processedLog = processLog(log);
  state.refresh++;
}

function checkOnlyThis(group, value) {
  const { selected } = state;
  for (const [g, o] of Object.entries(selected)) {
    for (const v of Object.keys(o)) {
      if (g == group && v == value) {
        state.selected[g][v].checked = true;
      } else if (g == group) {
        state.selected[g][v].checked = false;
      }
    }
  }
  processedLog = processLog(log);
  state.refresh++;
}

function MultiValue(props) {
  const onMouseDown = (e) => {
    e.preventDefault();
    e.stopPropagation();
  };
  const innerProps = { ...props.innerProps, onMouseDown };
  return <components.MultiValue {...props} innerProps={innerProps} />;
}

function MultiValueLabel(props) {
  const { selected } = useSnapshot(state);
  const { data: { group, value } } = props;
  return (
    <components.MultiValueLabel {...props}>
      <Checkbox
        checked={selected[group][value]?.checked}
        onChange={() => onCheckboxChange(group, value)}
      />
      <Button onClick={() => checkOnlyThis(group, value)} />
      <span {...props} />
    </components.MultiValueLabel>
  );
}

function SelectInput(props) {
  return (
    <components.Input
      {...props}
      onSelect={() => {
        state.isMenuOpen = true;
      }}
      onBlur={() => {
        state.isMenuOpen = false;
      }}
    />
  );
}

function arrayMove(array, from, to) {
  const slicedArray = array.slice();
  slicedArray.splice(
    to < 0 ? array.length + to : to,
    0,
    slicedArray.splice(from, 1)[0],
  );
  return slicedArray;
}

function onSortEnd({ oldIndex, newIndex }) {
  const { sortSelected } = state;
  state.sortSelected = arrayMove(sortSelected, oldIndex, newIndex);
}

const SortableSelect = SortableContainer(CreatableSelect);

function Select() {
  const { select, isMenuOpen, sortSelected } = useSnapshot(state);
  const level = Array.from(select.level).sort();
  const inferredLevel = Array.from(select.inferredLevel).sort();
  const host = Array.from(select.host).sort();
  const pod = Array.from(select.pod).sort();
  const container = Array.from(select.container).sort();
  const options = [
    {
      label: "Level",
      options: level.map((value) => ({
        label: value ? value.toUpperCase() : value + "UNDEFINED",
        group: "level",
        value: value ? value.toUpperCase() : value, // TODO: check
      })),
    },
    {
      label: "Inferred Level",
      options: inferredLevel.map((value) => ({
        label: value ? value : value + "undefined",
        group: "inferredLevel",
        value: value,
      })),
    },
    {
      label: "Hosts",
      options: host.map((value) => ({ label: value, group: "host", value })),
    },
    {
      label: "Pods",
      options: pod.map((value) => ({ label: value, group: "pod", value })),
    },
    {
      label: "Containers",
      options: container.map((value) => ({
        label: value,
        group: "container",
        value,
      })),
    },
  ];
  // https://react-select.com/advanced#sortable-multiselect
  return (
    <SortableSelect
      useDragHandle
      axis="xy"
      onSortEnd={onSortEnd}
      distance={4}
      backspaceRemovesValue={false}
      isMulti
      menuIsOpen={isMenuOpen}
      onKeyDown={onKeyDown}
      value={sortSelected}
      options={options}
      styles={styles}
      onChange={onChange}
      getHelperDimensions={({ node }) => node.getBoundingClientRect()}
      components={{
        Input: SelectInput,
        MultiValue: SortableElement(MultiValue),
        MultiValueLabel: SortableHandle(MultiValueLabel),
      }}
    />
  );
}

function Header() {
  const { isAnd, isNot, searchText } = useSnapshot(state);
  return (
    <div>
      <span>Log</span>
      <Button onClick={onFollow}>
        Follow
      </Button>
      <Button onClick={onSort}>
        Sort
      </Button>
      <Input value={searchText} onChange={onInput} />
      <Checkbox
        label="And"
        style={{ marginRight: "1em" }}
        checked={isAnd}
        onChange={() => {
          state.isAnd = !isAnd;
          processedLog = processLog(log);
          state.refresh++;
        }}
      />
      <Checkbox
        label="Not"
        style={{ marginRight: "1em" }}
        checked={isNot}
        onChange={() => {
          state.isNot = !isNot;
          processedLog = processLog(log);
          state.refresh++;
        }}
      />
      <Status />
    </div>
  );
}

function TableGrid({ height }) {
  const { follow, refresh } = useSnapshot(state); // refresh is important
  const ref = React.useRef();
  if (ref?.current && follow && processedLog.length > 100) {
    ref.current.scrollToItem({
      rowIndex: processedLog.length,
    });
  }
  return (
    <VariableSizeGrid
      itemData={processedLog}
      ref={ref}
      onScroll={onScroll}
      overscanRowCount={10}
      columnCount={log[0]?.cols?.length ?? 0}
      columnWidth={(index) => {
        const widths = {
          host: 40,
          pod: 60,
          container: 30,
          ts: 100,
          msg: 1000,
          level: 10,
          inferredLevel: 10,
        };
        const { cols } = log[0];
        return widths[cols[index]] ?? 1000;
      }}
      height={height}
      rowCount={processedLog.length}
      rowHeight={() => 20}
      width={window.innerWidth}
    >
      {Cell}
    </VariableSizeGrid>
  );
}

function Log({ opts }) {
  const headerRef = React.useRef();
  const selectRef = React.useRef();
  const { init } = useSnapshot(state);

  React.useEffect(() => {
    if (init) {
      initFun(opts);
      state.init = false;
    }
  });

  const headerHeight = (headerRef?.current?.clientHeight ?? 0) +
    (selectRef?.current?.clientHeight ?? 0);
  const logHeight = window.innerHeight - headerHeight - 60;
  return (
    <>
      <style>{style}</style>
      <Tabs>
        <TabList style={{ margin: 0 }} ref={headerRef}>
          <Tab style={{ padding: 0 }}>
            <Header />
          </Tab>
          <Tab style={{ padding: 0 }}>History</Tab>
        </TabList>
        <TabPanel>
          <Select ref={selectRef} />
          <TableGrid height={logHeight} />
        </TabPanel>
        <TabPanel>
          History
        </TabPanel>
      </Tabs>
    </>
  );
}

export default function render(opts) {
  const app = <Log opts={opts} />;
  ReactDOM.createRoot(document.getElementById("app")).render(app);
}
