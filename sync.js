import {
  dirname,
  join,
  relative,
  resolve,
} from "https://deno.land/std@0.148.0/path/posix.ts";
import { walk } from "https://deno.land/std@0.148.0/fs/walk.ts";
import { pathToRegexp } from "https://deno.land/x/path_to_regexp@v6.2.1/index.ts";
import { alive } from "./proc.js";
import Storage from "./storage.js";
import { readLines } from "https://deno.land/std@0.148.0/io/buffer.ts";
import { readerFromStreamReader } from "https://deno.land/std@0.148.0/streams/conversion.ts";

const storage = new Storage(import.meta.url);
const args = new URL(import.meta.url).searchParams.get("watch");

if (args) {
  await watch(...JSON.parse(args));
}

async function syncOnce(modPath, fun, args) {
  const mod = await import(modPath);
  const res = await mod[fun](args);
  await Promise.all(
    res.map(({ cwd, src, dest, exclude, hooks }) =>
      syncOneOnce(src, dest, { exclude, hooks, cwd })
    ),
  );
}

async function syncOneOnce(src0, dest0, opts = {}) {
  const { exclude = [], cwd = Deno.cwd(), hooks = [] } = opts;
  const src = resolve(cwd, src0);
  const dest = resolve(cwd, dest0);
  const skip = exclude.map((e) =>
    pathToRegexp(join(src, e), [], { end: false })
  );
  await Deno.mkdir(dest, { recursive: true });
  const tasks = [];
  const mkdirTasks = {};
  for await (
    const { path: srcPath, isFile, isDirectory } of walk(src, { skip })
  ) {
    const relSrc = relative(src, srcPath);
    const destPath = join(dest, relSrc);
    if (isFile) {
      tasks.push(copy(srcPath, destPath, mkdirTasks, cwd, hooks));
    } else if (isDirectory && !mkdirTasks[destPath]) {
      mkdirTasks[destPath] = Deno.mkdir(destPath, { recursive: true });
    }
  }
  await Promise.all(tasks + Object.values(mkdirTasks));
}

async function copy(src, dest, mkdirTasks, cwd, hooks) {
  const srcInfo = await Deno.stat(src);
  let destInfo;
  try {
    destInfo = await Deno.stat(dest);
  } catch {
    // ignore
  }
  if (!destInfo || srcInfo.mtime > destInfo.mtime) {
    const dir = dirname(dest);
    if (!mkdirTasks[dir]) {
      mkdirTasks[dir] = Deno.mkdir(dir, { recursive: true });
    }
    await mkdirTasks[dir];
    await copyFile(src, dest, { cwd, hooks });
  }
}

export async function stopSync(mod, fun) {
  const prev = storage[fun + "@" + mod];
  if (prev && await alive(prev)) {
    Deno.kill(prev, "SIGKILL");
  }
}

export async function startSync(mod, fun, args, opts) {
  const { importMap } = opts;
  await syncOnce(mod, fun, args, opts);
  const prev = storage[fun + "@" + mod];
  if (prev && await alive(prev)) {
    Deno.kill(prev, "SIGKILL");
  }
  const watch = JSON.stringify([mod, fun, args]);
  const urlParams = { watch };
  const url = import.meta.url + "?" + new URLSearchParams(urlParams);
  const args2 = ["run", "--unstable", "-A"];
  if (importMap) {
    args2.push("--import-map");
    args2.push("'" + importMap + "'");
  }
  args2.push("'" + url + "'");
  const cmd = [Deno.execPath(), ...args2].join(" ");
  const child = Deno.spawnChild("sh", {
    args: ["-c", `${cmd} &! echo "@@PID_$!_PID@@"`],
    stdout: "piped",
  });
  let pid;
  for await (
    const line of readLines(readerFromStreamReader(child.stdout.getReader()))
  ) {
    const match = line.match(/^@@PID_(\d+)_PID@@$/);
    if (match) {
      pid = match[1];
      break;
    }
  }
  child.unref();
  storage[fun + "@" + mod] = pid;
}

async function watch(modPath, fun, args) {
  const mod = await import(modPath);
  const res = await mod[fun](args);
  await Promise.all(
    res.map(({ cwd, src, dest, exclude, hooks }) =>
      watchOne(cwd, src, dest, exclude, hooks)
    ),
  );
}

async function watchOne(cwd, src0, dest0, exclude = [], hooks = {}) {
  const src = resolve(cwd, src0);
  const dest = resolve(cwd, dest0);
  const skips = exclude.map((e) =>
    pathToRegexp(join(src, e), [], { end: false })
  );
  for await (const event of Deno.watchFs(src)) {
    const { paths, kind, flag } = event;
    for (const srcFile of paths) {
      if (!skips.some((s) => s.test(srcFile))) {
        const destFile = join(dest, relative(src, srcFile));
        switch (kind) {
          // https://docs.rs/notify/5.0.0-pre.2/notify/event/enum.EventKind.html
          case "create": {
            let isDirectory;
            try {
              const stat = await Deno.stat(srcFile);
              isDirectory = stat.isDirectory;
            } catch {
              break; // not found
            }
            if (isDirectory) {
              await Deno.mkdir(destFile, { recursive: true });
            } else {
              try {
                await copyFile(srcFile, destFile, { cwd, hooks });
              } catch {
                break; // not found
              }
            }
            break;
          }
          case "modify": {
            let isFile;
            try {
              const stat = await Deno.stat(srcFile);
              isFile = stat.isFile;
            } catch {
              break; // not found
            }
            if (isFile) {
              try {
                await copyFile(srcFile, destFile, { cwd, hooks });
              } catch {
                break; // not found
              }
            }
            break;
          }
          case "remove":
            try {
              await Deno.remove(destFile, { recursive: true });
            } catch {
              break; // not found
            }
            break;
          case "other":
            // https://doc.deno.land/deno/stable/~/Deno.FsEventFlag
            // https://docs.rs/notify/5.0.0-pre.2/notify/event/enum.Flag.html
            if (flag === "rescan") {
              await syncOnce(src, dest, { exclude, cwd, hooks });
            }
            break;
          case "access": // non-mutating access operations on files.
          case "any": // catch-all event kind, for unsupported/unknown events.
          default:
        }
      }
    }
  }
}

async function copyFile(src, dest, opts = {}) {
  const { cwd = Deno.cwd(), hooks = {} } = opts;
  const matched = [];
  for (const [path, fun] of Object.entries(hooks)) {
    const reg = pathToRegexp(resolve(cwd, path), [], { end: false });
    if (reg.test(src)) {
      matched.push(fun);
    }
  }
  if (matched.length > 0) {
    let text = await Deno.readTextFile(src);
    let dest2 = dest;
    for (const fun of matched) {
      const { file, path } = fun;
      if (file) {
        text = await file(text);
      } else {
        text = await fun(text);
      }
      if (path) {
        dest2 = await path(dest2);
      }
    }
    await Deno.writeTextFile(dest2, text);
  } else {
    await Deno.copyFile(src, dest);
  }
}
