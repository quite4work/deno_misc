import ColorHash from "https://deno.land/x/color_hash@v2.0.1/mod.ts";
import x256 from "https://deno.land/x/x256@v0.2.0/mod.ts";
import {
  blue,
  cyan,
  green,
  inverse,
  magenta,
  red,
  yellow,
} from "https://deno.land/x/kleur@v0.0.1-alpha.0/colors.ts";

export function parse(text) {
  try {
    return JSON.parse(text);
  } catch {
    const match = text.match(re.klog());
    if (match) {
      return klog(match);
    } else {
      const match = text.match(re.logfmt());
      if (match) {
        try {
          return logfmt.parse(text);
        } catch {
          // TODO: remove color
          return inverse(red("??")) + " " + text;
        }
      } else {
        return text;
      }
    }
  }
}

// print

export function colorCode(text) {
  const colorHash = new ColorHash();
  return x256(...colorHash.rgb(text));
}

export function color(text, code) {
  const c = code ?? colorCode(text);
  return `\x1b[38;5;${c}m${text}\x1b[0m`;
}

export function printJson(obj, { notFields = [] } = {}) {
  let res = cyan("{");
  for (const [k, v] of Object.entries(obj)) {
    let vStr;
    if (_.isPlainObject(v)) {
      vStr = printJson(v);
    } else if (
      Array.isArray(notFields) && notFields.some((p) => p(v, k, obj))
    ) {
      continue;
    } else if (_.isBoolean(v)) {
      vStr = yellow(v);
    } else if (_.isNumber(v)) {
      vStr = blue(v);
    } else if (Array.isArray(v)) {
      vStr = cyan("[");
      for (const e of v) {
        vStr += `${e} `;
      }
      vStr = vStr.trim() + cyan("]");
    } else if (!v) {
      // TODO: differentiate: null, undefined, null, false, etc
      continue;
    } else if (typeof v === "string") {
      vStr = v.trim();
    } else {
      vStr = v.toString();
    }
    const kColor = magenta(k);
    res += `${kColor}${cyan(":")}${vStr} `;
  }
  if (res === cyan("{")) {
    return "";
  } else {
    return res.trim() + cyan("}");
  }
}

export function formatLevel(v, defaultLevel = "info") {
  switch (v) {
    case "info":
      return green("I");
    case "debug":
      return magenta("D");
    case "warning":
      return inverse(yellow("W"));
    case "error":
      return inverse(red("E"));
  }
  switch (inferLevel(v, defaultLevel)) {
    case "info":
      return green("i");
    case "debug":
      return magenta("d");
    case "warning":
      return inverse(yellow("w"));
    case "error":
      return inverse(red("e"));
  }
}
