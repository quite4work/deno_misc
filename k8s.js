export function configMap(name, data) {
  return {
    apiVersion: "v1",
    kind: "ConfigMap",
    metadata: {
      name,
      labels: {
        my_test: "true",
      },
    },
    data,
  };
}

export function secret(name, data) {
  return {
    apiVersion: "v1",
    kind: "Secret",
    metadata: {
      name,
    },
    stringData: data,
  };
}

export function cronJob(
  name,
  image,
  command,
  {
    schedule = "* * * * *",
    suspend = false,
    port,
    config,
    concurrencyPolicy = "Allow",
  } = {},
) {
  const spec = {
    restartPolicy: "OnFailure",
  };
  const container = {
    name,
    image,
    imagePullPolicy: "IfNotPresent",
    command,
  };
  if (config) {
    container.volumeMounts = [{
      name: "config",
      mountPath: "/conf",
    }];
    spec.volumes = [
      {
        name: "config",
        configMap: {
          name: config,
        },
      },
    ];
  }
  if (port) {
    container.ports = [{
      name: "default",
      containerPort: port,
    }];
  }
  spec.containers = [container];
  return {
    apiVersion: "batch/v1",
    kind: "CronJob",
    metadata: {
      name,
      labels: {
        "test-cron": "true",
      },
    },
    spec: {
      concurrencyPolicy,
      suspend,
      schedule,
      jobTemplate: {
        spec: {
          ttlSecondsAfterFinished: 0,
          template: {
            metadata: {
              name,
              labels: {
                my_test_pod: "cron",
                my_test: "true",
              },
            },
            spec,
          },
        },
      },
    },
  };
}

export function pvc(name, storage, { storageClassName = "default" }) {
  return {
    apiVersion: "v1",
    kind: "PersistentVolumeClaim",
    metadata: {
      name,
    },
    spec: {
      storageClassName,
      accessModes: [
        "ReadWriteOnce",
      ],
      resources: {
        requests: {
          storage,
        },
      },
    },
  };
}
export function pod(
  name,
  {
    image,
    command,
    volumes = [],
    volumeMounts = [],
    hostPaths = [],
    ports,
    config,
    pvc,
  },
) {
  const container = { name, image, ports: [], volumeMounts };
  if (!image) {
    container.image = "alpine";
    container.command = ["tail", "-f", "/etc/hostname"];
  }
  if (command) {
    container.command = command;
  }
  if (pvc) {
    for (const [name, mountPath] of Object.entries(pvc)) {
      volumes.push({ name, persistentVolumeClaim: { claimName: name } });
      volumeMounts.push({ mountPath, name });
    }
  }
  for (const { name, hostPath, mountPath } of hostPaths) {
    volumes.push({ name, hostPath: { path: hostPath } });
    volumeMounts.push({ name, mountPath });
  }
  if (config) {
    volumes.push({
      name: "config",
      configMap: {
        name: config,
      },
    });
    container.volumeMounts.push({
      name: "config",
      mountPath: "/conf",
    });
  }
  if (ports) {
    for (const [name, containerPort] of Object.entries(ports)) {
      container.ports.push({ name, containerPort });
    }
  }
  return {
    kind: "Pod",
    apiVersion: "v1",
    metadata: {
      name,
      labels: {
        my_test_pod: name,
        my_test: "true",
      },
    },
    spec: {
      restartPolicy: "OnFailure",
      volumes,
      containers: [container],
    },
  };
}

export function service(
  name,
  pod,
  { port = 80, targetPort, nodePort, clusterIP, externalIPs = [] } = {},
) {
  const spec = {
    type: nodePort ? "NodePort" : "ClusterIP",
    selector: {
      my_test_pod: pod,
    },
    ports: [{ port }],
    externalIPs,
  };
  if (targetPort) {
    spec.ports[0].targetPort = targetPort;
  }
  if (nodePort) {
    spec.ports[0].nodePort = nodePort;
  }
  if (clusterIP) {
    spec.clusterIP = clusterIP;
  }
  return {
    kind: "Service",
    apiVersion: "v1",
    metadata: {
      name,
      labels: {
        my_test: "true",
      },
    },
    spec,
  };
}

export function daemonSet(name, opts) {
  const { hostPort } = opts;
  const { spec } = pod(name, opts);
  if (hostPort) {
    for (const port of spec.containers[0].ports) {
      if (port.containerPort === hostPort) {
        port.hostPort = hostPort;
      }
    }
  }
  spec.restartPolicy = "Always";
  return {
    apiVersion: "apps/v1",
    kind: "DaemonSet",
    metadata: { name },
    spec: {
      selector: { matchLabels: { name } },
      template: { metadata: { labels: { name } }, spec },
    },
  };
}

export function ingress(name, path, service, port) {
  return {
    apiVersion: "networking.k8s.io/v1",
    kind: "Ingress",
    metadata: {
      name,
      labels: {
        my_test: "true",
      },
      annotations: {
        // "nginx.ingress.kubernetes.io/rewrite-target": "/",
        // "nginx.ingress.kubernetes.io/proxy-read-timeout": "10000",
        // "nginx.ingress.kubernetes.io/proxy-send-timeout": "10000",
        // "acme.cert-manager.io/http01-edit-in-place": "true",
        "kubernetes.io/tls-acme": "true",
      },
    },
    spec: {
      tls: [
        {
          hosts: [name],
          secretName: `${name}.tls`,
        },
      ],
      rules: [
        {
          http: {
            paths: [
              {
                path,
                pathType: "Prefix",
                backend: {
                  service: {
                    name: service,
                    port: {
                      number: port,
                    },
                  },
                },
              },
            ],
          },
        },
      ],
    },
  };
}
