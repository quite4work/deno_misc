import { DEFAULT_SCHEMA } from "https://deno.land/std@0.148.0/encoding/yaml.ts";
import { Type } from "https://deno.land/std@0.148.0/encoding/_yaml/type.ts";
import { promptSecret } from "https://cdn.jsdelivr.net/gh/quite4work/deno-prompts@0.0.4/mod.ts";
import * as cache from "./cache.js";
import * as sexpect from "./sexpect.js";
import { stringify } from "./yaml.js";
import { paramCase } from "https://deno.land/x/case@2.1.1/mod.ts";
import { merge } from "https://cdn.skypack.dev/lodash-es@4.17.21";

// DWIM function
export async function getPass() {
  let pass = Deno.env.get("ANSIBLE_VAULT_PASSWORD_FILE");
  if (pass) {
    const p = Deno.run({ cmd: [pass], stdout: "piped" });
    const out = await p.output();
    pass = new TextDecoder().decode(out).trim();

    // unset these variables to fix bugs in some playbooks
    Deno.env.delete("ANSIBLE_VAULT_PASSWORD_FILE");
    Deno.env.delete("ANSIBLE_ASK_VAULT_PASS");
  } else {
    pass = await promptSecret("Vault password:");
  }
  return pass;
}

export function vault(strings, ...args) {
  let result = strings[0];
  for (let i = 1; i < strings.length; i++) {
    result += args[i - 1].toString();
    result += strings[i];
  }
  return result.trim();
}

export async function mergeExtraVars(vars) {
  const extraVars = await cache.get("ansibleExtraVars");
  if (extraVars) {
    const merged = merge(extraVars, vars);
    await cache.set("ansibleExtraVars", merged);
  } else {
    await cache.set("ansibleExtraVars", vars);
  }
}

export async function getExtraVars() {
  const extraVars = await cache.get("ansibleExtraVars");
  return extraVars ?? {};
}

export async function play(playbook, opts) {
  const {
    vaultPass,
    expect: isExpect,
    env,
    tags,
    extraVars,
    inventory,
  } = opts;
  const ansibleOpts = { tags, i: inventory };
  const ansibleExtraVars = await cache.get("ansibleExtraVars");
  const tmp = "/tmp/dans.yml";
  await Deno.writeTextFile(tmp, "");
  const extra = {};
  if (ansibleExtraVars) {
    ansibleOpts.e = [`@${tmp}`];
    const ansibleExtraVars2 = {};
    for (const [k, v] of Object.entries(ansibleExtraVars)) {
      if (k.startsWith("$")) {
        extra[k] = v;
      } else {
        ansibleExtraVars2[k] = v;
      }
    }
    const res = stringify(ansibleExtraVars2) + `\n\n` +
      Object.entries(extra)
        .map(([k, v]) => `\n# ${k}\n\n${v}`).join("\n\n");
    await Deno.writeTextFile(tmp, res);
  }
  const args = [
    playbook,
    ...optsToStr(ansibleOpts),
    ...optsToStr({ e: processExtraVars(extraVars) }),
  ];
  const sock = await sexpect.spawn("ansible-playbook", { args, env });
  await sexpect.expect(sock, "Vault password:");
  await sexpect.send(sock, vaultPass);
  if (isExpect) {
    return sock;
  } else {
    return await sexpect.exit(sock);
  }
}

function optsToStr(opts) {
  return Object.entries(opts).flatMap(([k, v1]) => {
    if (v1 === undefined) {
      return [];
    }
    const v2 = Array.isArray(v1) ? v1 : [v1];
    return v2.flatMap((v) =>
      k.length == 1 ? [`-${k}`, v] : [`--${paramCase(k)}`, v]
    );
  });
}

function processExtraVars(vars) {
  if (!vars) {
    return [];
  } else if (Array.isArray(vars)) {
    return vars.flatMap(extraVars);
  } else {
    return [extraVars(vars)];
  }
}

function extraVars(vars) {
  if (typeof vars == "string" && vars.startsWith("@")) {
    return [vars];
  } else {
    const kv = [];
    const json = [];
    for (const [k, v] of Object.entries(vars)) {
      if (typeof v == "string") {
        kv.push(`${k}=${v}`);
      } else {
        json.push(JSON.stringify(vars));
      }
    }
    kv.push(...json);
    return kv;
  }
}

const VaultType = new Type("!vault", {
  kind: "scalar",
  resolve(data) {
    return /^\$ANSIBLE_VAULT/.test(data);
  },
  predicate(data) {
    return /^\$ANSIBLE_VAULT/.test(data);
  },
});

export const ansibleYamlSchema = DEFAULT_SCHEMA.extend({
  explicit: [VaultType],
});
