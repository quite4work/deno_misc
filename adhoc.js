export function list(strings, ...keys) {
  const text = text.lit(strings, ...keys);
  const noComments = text.replaceAll(/\s*\/\/.*/g, "");
  return noComments.split("\n").map((s) => s.trim()).filter((s) =>
    s.length > 0
  );
}
