import { decrypt, encrypt, hash } from "./crypt.js";

// NOTE: localStorage is not supported by `deno compile`

export async function open(name) {
  const context = await hash(import.meta.url + name);

  return {
    async get(key) {
      return await get(context, key);
    },

    async set(key, val) {
      await set(context, key, val);
    },

    async delete(key) {
      await remove(context, key);
    },
  };
}

async function get(context, key) {
  const hsh = await hash(context + key);
  const key2 = await hash(await hash(hsh + context + key) + hsh);
  const cipher = localStorage.getItem(`${context}_${hsh}`);
  if (cipher) {
    return JSON.parse(await decrypt(key2, cipher));
  }
}

async function remove(context, key) {
  const hsh = await hash(context + key);
  localStorage.removeItem(`${context}_${hsh}`);
}

async function set(context, key, val) {
  if (!val && val !== false) {
    // Do not save empty values unless it is boolean `false`
    return;
  }
  const hsh = await hash(context + key);
  const key2 = await hash(await hash(hsh + context + key) + hsh);
  const cipher = await encrypt(key2, JSON.stringify(val));
  localStorage.setItem(`${context}_${hsh}`, cipher);
}
