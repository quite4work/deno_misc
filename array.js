export function chunks(a, n) {
  return [...Array(Math.ceil(a.length / n))].map((_, i) =>
    a.slice(n * i, n + n * i)
  );
}
