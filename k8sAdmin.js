import { pod, pvc } from "./k8s.js";
import { apply } from "./kubectl.js";

export function k8sAdminYargsCommands(opts) {
  return [
    {
      command: "use-pvc <name> <size>",
      desc: "",
      builder: {
        namespace: {
          alias: "n",
          type: "string",
          description: "Deploy to namespace",
        },
        storageClassName: {
          alias: "sc",
          type: "string",
        },
      },
      handler: (argv) => usePvc(argv.name, argv.size, { ...opts, ...argv }),
    },
  ];
}

async function usePvc(name, size, opts) {
  const { context, namespace, storageClassName } = opts;
  const pvc2 = pvc(name, size, { storageClassName });
  const pvc3 = pvc(name + "2", size, { storageClassName });
  const pod2 = pod(name, { pvc: { "/data": name, "/data2": name + "2" } });
  await apply([pvc2, pvc3, pod2], {
    context,
    namespace,
  });
}
