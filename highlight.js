import x256 from "https://deno.land/x/x256@v0.2.0/mod.ts";
import hexRgb from "https://raw.githubusercontent.com/sindresorhus/hex-rgb/main/index.js";
import { escapeRegExp, isRegExp } from "https://cdn.skypack.dev/lodash-es@4.17.21";

export function cli(text, patterns, color) {
  let res = text;
  const insert = {};
  const regexes = patterns.map((pattern) => {
    if (isRegExp(pattern)) {
      return pattern;
    } else {
      return escapeRegExp(pattern);
    }
  });
  const regex = new RegExp(`(${regexes.join("|")})`, "dg");
  for (const match of text.matchAll(regex)) {
    for (const v of match.indices.slice(1)) {
      let o = "", c = "";
      if (color) {
        if (typeof color === "string") {
          const { red, green, blue } = hexRgb(color);
          const color2 = x256([red, green, blue]);
          o = `\x1b[38;5;${color2}m`;
          c = `\x1b[0m`;
        } else if (Array.isArray(color)) {
          const color2 = x256(color);
          o = `\x1b[38;5;${color2}m`;
          c = `\x1b[0m`;
        }
      }
      if (v) {
        if (insert[v[0]]) {
          insert[v[0]] = `${c}${o}`;
        } else {
          insert[v[0]] = `${o}`;
        }
        if (insert[v[1]]) {
          insert[v[1]] = `${c}${o}`;
        } else {
          insert[v[1]] = `${c}`;
        }
      }
    }
  }
  res = insertTextAtIndices(res, insert);
  return res;
}

export function cliGroups(text, regex, colors) {
  let res = text;
  const insert = {};
  for (const match of text.matchAll(new RegExp(regex, "dg"))) {
    for (const [k, v] of Object.entries(match.indices.groups)) {
      const color = colors[k];
      // if (typeof color === "function") {
      //   res = color(res);
      // } else {
      let o = "", c = "";
      if (color) {
        if (typeof color === "string") {
          const { red, green, blue } = hexRgb(color);
          const color2 = x256([red, green, blue]);
          o = `\x1b[38;5;${color2}m`;
          c = `\x1b[0m`;
        } else if (Array.isArray(color)) {
          const color2 = x256(color);
          o = `\x1b[38;5;${color2}m`;
          c = `\x1b[0m`;
        }
      }
      if (v) {
        if (insert[v[0]]) {
          insert[v[0]] = `${c}${o}`;
        } else {
          insert[v[0]] = `${o}`;
        }
        if (insert[v[1]]) {
          insert[v[1]] = `${c}${o}`;
        } else {
          insert[v[1]] = `${c}`;
        }
      }
      // }
    }
    res = insertTextAtIndices(res, insert);
  }
  return res;
}

export function html(text, regex, colors) {
  let res = text;
  const insert = {};
  for (const match of text.matchAll(new RegExp(regex, "dg"))) {
    for (const [k, v] of Object.entries(match.indices.groups)) {
      const color = colors[k];
      // if (typeof color === "function") {
      //   res = color(res);
      // } else {
      let o = "", c = "";
      if (color) {
        o = `<span style="color:${color}">`;
        c = `</span>`;
      }
      if (v) {
        if (insert[v[0]]) {
          insert[v[0]] = `${c}${o}`;
        } else {
          insert[v[0]] = `${o}`;
        }
        if (insert[v[1]]) {
          insert[v[1]] = `${c}${o}`;
        } else {
          insert[v[1]] = `${c}`;
        }
      }
      // }
    }
    res = insertTextAtIndices(res, insert);
  }
  return res;
}

function insertTextAtIndices(str, text) {
  const entr = Object.entries(text);
  const chanks = splitAt(str, ...entr.map(([k]) => Number(k)));
  let res = "";
  for (const [i, v] of entr.map(([_, v], i) => [i, v])) {
    res += chanks[i] + v;
  }
  res += chanks[chanks.length - 1];
  return res;
}

function splitAt(slicable, ...indices) {
  return [0, ...indices].map((n, i, m) => slicable.slice(n, m[i + 1]));
}

function replace(str, replacement, cutStart, cutEnd) {
  return str.substr(0, cutStart) + replacement + str.substr(cutEnd + 1);
}
