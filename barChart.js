function bar(value, maxValue, maxBarLength) {
  const fractions = ["▏", "▎", "▍", "▋", "▊", "▉"];
  const barLength = value * maxBarLength / maxValue;
  const wholeNumberPart = Math.floor(barLength);
  const fractionalPart = barLength - wholeNumberPart;
  let bar = fractions[fractions.length - 1].repeat(wholeNumberPart);
  if (fractionalPart > 0) {
    bar += fractions[Math.floor(fractionalPart * fractions.length)];
  }
  return bar;
}

export function draw(data, opts = {}) {
  const { hideValues, orderByKey, max } = opts;
  const { columns } = Deno.consoleSize(Deno.stdout.rid);
  const formatted = Object.keys(data).map((key) => ({
    key: key,
    value: data[key],
  }));
  const sorted = formatted.sort((a, b) =>
    orderByKey ? b.key - a.key : b.value - a.value
  );
  let maxValue = max ?? Math.max(...sorted.map((item) => item.value));
  maxValue = maxValue > columns ? maxValue : columns;
  const maxKeyNameLength = Math.max(...sorted.map((item) => item.key.length));
  const maxValueLength = hideValues ? 0 : Math.max(
    ...sorted.map((item) => item.value.toString().length),
  );
  const {
    length = columns - maxKeyNameLength - maxValueLength,
  } = opts;
  return sorted.map((item) => {
    const prefix = item.key +
      " ".repeat(maxKeyNameLength - item.key.length);
    const barText = bar(item.value, maxValue, length);
    const suffix = hideValues ? "" : `${item.value}`;
    return prefix + barText + suffix;
  }).join("\n");
}
