import err from "./err.js";
import { hash } from "./crypt.js";

export function dynuDnsYargsCommands(opts) {
  const { domain } = opts;
  return [
    {
      command: "dynudns update <ip>",
      desc: "Kubectl",
      builder: {
        verbosity: { alias: "v", type: "number" },
        domain: { alias: "d", demandOption: true, default: domain },
      },
      handler: (args) => updateIp(args.domain, args.ip, { ...opts, ...args }),
    },
  ];
}

async function getDomains(opts) {
  const { domains } = await api("GET", "/dns", opts);
  return domains;
}

// recordType: A, AAAA, CAA, CNAME, HINFO, LOC, MX, NS, PTR, PF, RP, SPF, TXT, UF, URI
async function getRecords(domain, recordType, opts) {
  const res = await api("GET", `/dns/record/${domain}`, {
    ...opts,
    qs: { recordType },
  });
  return res;
}

// https://www.dynu.com/en-US/Support/API#/dns/dnsIdRecordPost
async function updateRecord(domain, body, opts) {
  const dmn = await getDomain(domain, opts);
  const res = await api("POST", `/dns/${dmn.id}/record`, {
    ...opts,
    body,
  });
  return res;
}

async function getDomain(domain, opts) {
  const domains = await getDomains(opts);
  const matchedDomains = domains.filter(({ name }) => domain === name);
  if (matchedDomains == 0) {
    throw `domain not found: ${domain} in: ${JSON.stringify(domains)}`;
  }
  if (matchedDomains > 1) {
    throw `multiple domain found: ${domain} in: ${JSON.stringify(domains)}`;
  }
  return matchedDomains[0];
}

async function updateDomain(domain, fields, opts) {
  const dmn = await getDomain(domain, opts);
  return await api("POST", `/dns/${dmn.id}`, {
    ...opts,
    body: { ...dmn, ...fields },
  });
}

async function api(method, path, opts) {
  const { verbosity, token, body, qs } = opts;
  const url = "https://api.dynu.com/v2" + path +
    (qs ? "?" + new URLSearchParams(qs) : "");
  if (verbosity > 1) {
    console.error(method, url);
    console.log(body);
  }
  const res = await fetch(url, {
    method,
    body: body ? JSON.stringify(body) : undefined,
    headers: { "API-Key": token, accept: "application/json" },
  });
  const resp = await res.json();
  if (verbosity) {
    console.error(resp);
  }
  if (resp.statusCode != 200) {
    throw err("request failed", { url, resp });
  }
  return resp;
}

export async function updateIp(domain, ip, opts = {}) {
  const { verbosity, password } = opts;
  const qs = {
    password: await hash(password, "SHA-256"),
    hostname: domain,
    myip: ip,
  };
  const url = "https://api.dynu.com/nic/update?" + new URLSearchParams(qs);
  if (verbosity > 1) {
    console.error("GET", url);
  }
  const res = await fetch(url, {
    method: "GET",
  });
  const resp = await res.text();
  if (verbosity) {
    console.error(resp);
  }
  const match = resp.match(new RegExp(`^(good ${ip}|nochg)$`));
  if (!match) {
    throw err("request failed", { url, resp });
  }
  return match[1] !== "nochg";
}

function setup(defaultOpts = {}) {
  const setup2 = (opts = {}) => setup({ ...defaultOpts, ...opts });
  setup2.updateDomain = (domain, fields, opts = {}) =>
    updateDomain(domain, fields, { ...defaultOpts, ...opts });
  setup2.updateIp = (domain, ip, opts = {}) =>
    updateIp(domain, ip, { ...defaultOpts, ...opts });
  setup2.getDomains = (opts = {}) => getDomains({ ...defaultOpts, ...opts });
  setup2.commands = (opts = {}) => commands({ ...defaultOpts, ...opts });
  setup2.getRecords = (domain, recordType, opts = {}) =>
    getRecords(domain, recordType, { ...defaultOpts, ...opts });
  setup2.updateRecord = (domain, body, opts = {}) =>
    updateRecord(domain, body, { ...defaultOpts, ...opts });
  return setup2;
}

export default setup();
