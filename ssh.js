// export async function scp(src, dest, hosts, opts) {
//   const stdin = await Deno.readTextFile(src);
//   return await dwim(`tee ${dest}`, hosts, { ...opts, stdin });
// }

export async function addToKnownHosts(ip) {
  const cmd = `ssh-keyscan -t ecdsa ${ip} >> ~/.ssh/known_hosts`;
  await Deno.spawn("sh", { args: ["-c", cmd] });
}

export async function removeFromKnownHosts(ip) {
  await Deno.spawn("ssh-keygen", { args: ["-R", ip] });
}

export async function updateConfig(host, fields) {
  const { hostName, user, identityFile } = fields;
  await initConfig();
  const conf = `Host ${host}
  HostName ${hostName}
  User ${user}
  IdentityFile ${identityFile}
`;
  await Deno.writeTextFile(
    Deno.env.get("HOME") + `/.ssh/config.d/${host}`,
    conf,
  );
}

async function initConfig() {
  try {
    await Deno.mkdir(Deno.env.get("HOME") + "/.ssh/config.d", {
      recursive: true,
    });
  } catch {
    // ignore if it is already exists
  }
  let config = "";
  try {
    config = await Deno.readTextFile(
      Deno.env.get("HOME") + "/.ssh/config",
    );
  } catch {
    // ignore if it is not exists
  }
  let includeExists;
  for (const line of config.split("\n")) {
    if (/Include config\.d\/\*/.test(line)) {
      includeExists = true;
      break;
    }
  }
  if (!includeExists) {
    await Deno.writeTextFile(
      Deno.env.get("HOME") + "/.ssh/config",
      "Include config.d/*\n\n" + config,
    );
  }
}
