import literal from "https://gitlab.com/xdeno/utils/-/raw/0.0.1/literal.js";
import yaml from "./yaml.js";

export function lit(strings, ...keys) {
  const strings2 = strings.map((x) =>
    x.replaceAll("{{", "'{{").replaceAll("}}", "}}'")
  );
  return yaml.parse(literal(strings2, ...keys));
}
