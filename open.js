// https://peter.sh/experiments/chromium-command-line-switches/
export async function chrome(url, args = {}) {
  const argsStr = argsToString({
    ...args,
    app: url,
    // Enable CORS support:
    // https://stackoverflow.com/questions/3102819/disable-same-origin-policy-in-chrome
    // - "user-data-dir": "/tmp/deno-chrome",
    // - "disable-web-security": true,
  });
  await Deno.spawn("open", {
    args: ["-na", "google chrome", "--args", argsStr],
  });
}

function argsToString(args) {
  return Object.entries(args).map(([k, v]) =>
    v === true ? `--${k}` : `--${k}=${JSON.stringify(v)}`
  ).join(" ");
}
